package testBlowUpMessage;

import blowUpMessage.BlowUpMessage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.swing.*;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class TestBlowUpMessage {
    private static final ByteArrayOutputStream consoleOutput = new ByteArrayOutputStream();
    @Before
    public void setUp() {
        System.setOut(new PrintStream(consoleOutput));
    }
    @After
    public void tearDown() {
        consoleOutput.reset();
    }
    private static Window findCurrentWindow() {
        Window [] windows = Window.getOwnerlessWindows();
        Window currentWindow = null;
        for (Window window: windows ) {
            if (((JFrame)window).getTitle().equals("Blow Up Message")) {
                currentWindow = window;
                break;
            }
        }
        return currentWindow;
    }
    private static ArrayList<Component> getAllComponents(final Container c) {
        Component[] comps = c.getComponents();
        ArrayList<Component> compList = new ArrayList<>();
        for (Component comp : comps) {
            compList.add(comp);
            if (comp instanceof Container)
                compList.addAll(getAllComponents((Container) comp));
        }
        return compList;
    }

    private static JLabel findMainLabel(Window window) {
        System.out.println("The window is "+window);
        JLabel mainLabel = null;
        ArrayList<Component> allComponents = getAllComponents(window);
        for (Component component : allComponents) {
            if (component.getName() != null &&
                    component.getName().equals("display-label")) {
                mainLabel = (JLabel) component;
                break;
            }
        }
        return mainLabel;
    }
    private static String getMainLabelText() {
        String str = "";
        if(findCurrentWindow() != null) {
            if(findMainLabel(findCurrentWindow()) != null &&
                    findMainLabel(findCurrentWindow()).getText() != null) {
                str = findMainLabel(findCurrentWindow()).getText();
            }
        }
        return str;
    }
    @Test
    public void itShouldBeTimeUp_whenCommandLineArgumentIsTimeUp() {
        BlowUpMessage.main(new String [] {"Time Up"});
        assertEquals("Time Up\n", consoleOutput.toString());
    }
    @Test
    public void itShouldShowInformationAboutWrongArgument_whenThereIsNoCommandLineArgument() {
        BlowUpMessage.main(new String [] {});
        assertEquals("Please Enter a message at the command line\n", consoleOutput.toString());
    }
    @Test
    public void itShouldShowAWindowWithLabelHavingTextTimeUp_whenCommandLineArgumentIsTimeUp() {
        BlowUpMessage.main(new String [] {"Time Up"});
        assertEquals("Time Up", getMainLabelText());
    }
}
