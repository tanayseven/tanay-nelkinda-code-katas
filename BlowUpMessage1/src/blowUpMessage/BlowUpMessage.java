package blowUpMessage;

import javax.swing.*;
import java.awt.*;

public class BlowUpMessage {
    private static void showTheWindow(String text) {
        JFrame frame = new JFrame();
        frame.setTitle("Blow Up Message");
        frame.setBounds(new Rectangle(0, 0, 500, 500));
        JLabel mainLabel = new JLabel();
        mainLabel.setName("display-label");
        mainLabel.setText(text);
        frame.add(mainLabel);
        frame.setVisible(true);
    }
    public static void main(String[] strings) {
        if(strings.length == 0) {
            System.out.println("Please Enter a message at the command line");
        } else {
            System.out.println(strings[0]);
            showTheWindow(strings[0]);
        }
    }
}
