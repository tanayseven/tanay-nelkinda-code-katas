package test;


import org.junit.Test;
import textToMorse.TextToMorse;

import static org.junit.Assert.assertEquals;

public class TestTextToMorse {
    @Test
    public void test_ConvertedTextForAShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterA() {
        assertEquals(TextToMorse.convert("A"), ".-");
    }
    @Test
    public void test_convertedTextForBShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterB() {
        assertEquals(TextToMorse.convert("B"), "-...");
    }
    @Test
    public void test_convertedTextForCShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterC() {
        assertEquals(TextToMorse.convert("C"), "-.-.");
    }
    @Test
    public void test_convertedTextForDShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterD() {
        assertEquals(TextToMorse.convert("D"), "-..");
    }
    @Test
    public void test_convertedTextForEShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterE() {
        assertEquals(TextToMorse.convert("E"), ".");
    }
    @Test
    public void test_convertedTextForFShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterF() {
        assertEquals(TextToMorse.convert("F"), "..-.");
    }
    @Test
    public void test_convertedTextForGShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterG() {
        assertEquals(TextToMorse.convert("G"), "--.");
    }
    @Test
    public void test_convertedTextForHShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterH() {
        assertEquals(TextToMorse.convert("H"), "....");
    }
    @Test
    public void test_convertedTextForIShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterI() {
        assertEquals(TextToMorse.convert("I"), "..");
    }
    @Test
    public void test_convertedTextForJShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterJ() {
        assertEquals(TextToMorse.convert("J"), ".---");
    }
    @Test
    public void test_convertedTextForKShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterK() {
        assertEquals(TextToMorse.convert("K"), "-.-");
    }
    @Test
    public void test_convertedTextForLShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterL() {
        assertEquals(TextToMorse.convert("L"), ".-..");
    }
    @Test
    public void test_convertedTextForMShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterM() {
        assertEquals(TextToMorse.convert("M"), "--");
    }
    @Test
    public void test_convertedTextForNShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterN() {
        assertEquals(TextToMorse.convert("N"), "-.");
    }
    @Test
    public void test_convertedTextForOShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterO() {
        assertEquals(TextToMorse.convert("O"), "---");
    }
    @Test
    public void test_convertedTextForPShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterP() {
        assertEquals(TextToMorse.convert("P"), ".--.");
    }
    @Test
    public void test_convertedTextForQShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterQ() {
        assertEquals(TextToMorse.convert("Q"), "--.-");
    }
    @Test
    public void test_convertedTextForRShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterR() {
        assertEquals(TextToMorse.convert("R"), ".-.");
    }
    @Test
    public void test_convertedTextForSShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterS() {
        assertEquals(TextToMorse.convert("S"), "...");
    }
    @Test
    public void test_convertedTextForTShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterT() {
        assertEquals(TextToMorse.convert("T"), "-");
    }
    @Test
    public void test_convertedTextForUShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterU() {
        assertEquals(TextToMorse.convert("U"), "..-");
    }
    @Test
    public void test_convertedTextForVShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterV() {
        assertEquals(TextToMorse.convert("V"), "...-");
    }
    @Test
    public void test_convertedTextForWShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterW() {
        assertEquals(TextToMorse.convert("W"), ".--");
    }
    @Test
    public void test_convertedTextForXShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterX() {
        assertEquals(TextToMorse.convert("X"), "-..-");
    }
    @Test
    public void test_convertedTextForYShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterY() {
        assertEquals(TextToMorse.convert("Y"), "-.--");
    }
    @Test
    public void test_convertedTextForZShouldBeItsMorseCode_whenTheStringIsOnlyOneCharacterZ() {
        assertEquals(TextToMorse.convert("Z"), "--..");
    }
}