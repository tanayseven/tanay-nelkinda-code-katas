package textToMorse;

import java.util.HashMap;

public class TextToMorse {

    private static HashMap<String, String> morseForText = new HashMap<>();
    static {
        morseForText.put("A", ".-");
        morseForText.put("B", "-...");
        morseForText.put("C", "-.-.");
        morseForText.put("D", "-..");
        morseForText.put("E", ".");
        morseForText.put("F", "..-.");
        morseForText.put("G", "--.");
        morseForText.put("H", "....");
        morseForText.put("I", "..");
        morseForText.put("J", ".---");
        morseForText.put("K", "-.-");
        morseForText.put("L", ".-..");
        morseForText.put("M", "--");
        morseForText.put("N", "-.");
        morseForText.put("O", "---");
        morseForText.put("P", ".--.");
        morseForText.put("Q", "--.-");
        morseForText.put("R", ".-.");
        morseForText.put("S", "...");
        morseForText.put("T", "-");
        morseForText.put("U", "..-");
        morseForText.put("V", "...-");
        morseForText.put("W", ".--");
        morseForText.put("X", "-..-");
        morseForText.put("Y", "-.--");
        morseForText.put("Z", "--..");
    }
    public static String convert(String str) {
        return morseForText.get(str);
    }
}