package morse_code_test;

import morse_code.MorseCode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by tanay on 9/8/16.
 */
@RunWith(Parameterized.class)
public class MorseCodeTest {

    private String inputString;
    private String expectedString;
    private MorseCode morseCode;

    @Before
    public void initialize() {
        morseCode = new MorseCode();
    }

    public MorseCodeTest(String inputString, String expectedString) {
        this.inputString = inputString;
        this.expectedString = expectedString;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {"A", ".-"}, {"B", "-..."}, {"C", "-.-."}, {"D", "-.."},{"E", "."}, {"F", "..-."}, {"G", "--."},
                {"H", "...."}, {"I", ".."}, {"J", ".---"}, {"K", "-.-"}, {"L", ".-.."}, {"M", "--"}, {"N", "-."},
                {"O", "---"}, {"P", ".--."}, {"Q", "--.-"}, {"R", ".-."}, {"S", "..."}, {"T", "-"}, {"U", "..-"},
                {"V", "...-"}, {"W", ".--"}, {"X", "-..-"}, {"Y", "-.--"}, {"Z", "--.."}
        });
    }

    @Test
    public void outputMorseShouldBeCorrectToTheInputMorse_whenSomeInputMorseIsGiven() {
        assertEquals(this.expectedString, morseCode.convert(this.inputString));
    }
}
