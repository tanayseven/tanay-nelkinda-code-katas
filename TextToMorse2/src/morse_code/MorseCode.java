package morse_code;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tanay on 9/8/16.
 */
public class MorseCode {

    private static class TextToMorse {
        public final static Map textToMorse = new HashMap<String, String>();
    }

    public MorseCode() {
        TextToMorse.textToMorse.putIfAbsent("A", ".-");
        TextToMorse.textToMorse.putIfAbsent("B", "-...");
        TextToMorse.textToMorse.putIfAbsent("C", "-.-.");
        TextToMorse.textToMorse.putIfAbsent("D", "-..");
        TextToMorse.textToMorse.putIfAbsent("E", ".");
        TextToMorse.textToMorse.putIfAbsent("F", "..-.");
        TextToMorse.textToMorse.putIfAbsent("G", "--.");
        TextToMorse.textToMorse.putIfAbsent("H", "....");
        TextToMorse.textToMorse.putIfAbsent("I", "..");
        TextToMorse.textToMorse.putIfAbsent("J", ".---");
        TextToMorse.textToMorse.putIfAbsent("K", "-.-");
        TextToMorse.textToMorse.putIfAbsent("L", ".-..");
        TextToMorse.textToMorse.putIfAbsent("M", "--");
        TextToMorse.textToMorse.putIfAbsent("N", "-.");
        TextToMorse.textToMorse.putIfAbsent("O", "---");
        TextToMorse.textToMorse.putIfAbsent("P", ".--.");
        TextToMorse.textToMorse.putIfAbsent("Q", "--.-");
        TextToMorse.textToMorse.putIfAbsent("R", ".-.");
        TextToMorse.textToMorse.putIfAbsent("S", "...");
        TextToMorse.textToMorse.putIfAbsent("T", "-");
        TextToMorse.textToMorse.putIfAbsent("U", "..-");
        TextToMorse.textToMorse.putIfAbsent("V", "...-");
        TextToMorse.textToMorse.putIfAbsent("W", ".--");
        TextToMorse.textToMorse.putIfAbsent("X", "-..-");
        TextToMorse.textToMorse.putIfAbsent("Y", "-.--");
        TextToMorse.textToMorse.putIfAbsent("Z", "--..");
    }

    public String convert(String inputString) {
        return (String) TextToMorse.textToMorse.get(inputString);
    }
}
