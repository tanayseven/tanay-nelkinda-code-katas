package testFileSize;


import fileSize.FileSize;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.nio.file.FileStore;

import static org.junit.Assert.assertEquals;

public class TestFileSize {
    private String fileName;
    private final ByteArrayOutputStream consoleOutput = new ByteArrayOutputStream();
    @Before
    public void createAFileOf256Bytes() {
        fileName = "someFile.bin";
        try {
            FileOutputStream outFile = new FileOutputStream(fileName);
            for(int i = 0 ; i < 256 ; ++i) {
                outFile.write(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Before
    public void setConsoleOutput() {
        System.setOut(new PrintStream(consoleOutput));
    }
    @Test
    public void fileSizeShouldBe256_whenChecked() {
        long size = FileSize.computeSizeOf(fileName);
        assertEquals(256, size);
    }
    @Test
    public void fileSizeShouldBe256_whenGivenAsCommandLineArgument() {
        FileSize.main(new String[] {fileName});
        assertEquals("256\n", consoleOutput.toString());
    }
    @Test
    public void itShouldBeAnError_whenGivenInputWrongFileNameAsArgument() {
        FileSize.main(new String[] {});
        assertEquals("Please give file name as command line argument\n", consoleOutput.toString());
    }
}
