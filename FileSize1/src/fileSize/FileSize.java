package fileSize;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by tanay on 11/8/16.
 */
public class FileSize {

    public static long computeSizeOf(String fileName) {
        int counter = 0;
        try {
            FileInputStream inFile = new FileInputStream(fileName);
            while (inFile.read() != -1) {
                counter += 1;
            }
            inFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return counter;
    }

    public static void main(String[] commandArguments) {
        long length = 0;
        if(commandArguments.length > 0) {
            length = computeSizeOf(commandArguments[0]);
            System.out.println(length);
        } else {
            System.out.println("Please give file name as command line argument");
        }
    }
}
