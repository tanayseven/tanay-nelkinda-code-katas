import org.junit.Test;

import calculator.Calculator;

import static org.junit.Assert.assertEquals;

public class CalculatorTester {
    static final double DEFAULT_PRECISION = 0.000001;

    @Test
    public void theCalculatorShouldHave0AsItsResult_whenTheCalculatorObjectIsJustCreated() {
        Calculator calculator = new Calculator();
        double result = calculator.resultantValue();
        assertEquals(0, result, DEFAULT_PRECISION);
    }
    @Test
    public void theCalculatorShouldHave0AsItsResult_whenTheCalculatorIsGivenAnInputNumber() {
        Calculator calculator = new Calculator();
        calculator.inputNumber(3.14159d);
        double result = calculator.resultantValue();
        assertEquals(0, result, DEFAULT_PRECISION);
    }
    @Test
    public void theCalculatorShouldHave1AsItsResult_whenWeAddTheInputtedNumber1() {
        Calculator calculator = new Calculator();
        calculator.inputNumber(1d);
        calculator.addNumber();
        double result = calculator.resultantValue();
        assertEquals(1, result, DEFAULT_PRECISION);
    }
    @Test
    public void theCalculatorShouldReturn3AsItsResult_whenWeFirstAdd1ThenAdd2() {
        Calculator calculator = new Calculator();
        calculator.inputNumber(1d);
        calculator.addNumber();
        calculator.inputNumber(2d);
        calculator.addNumber();
        double result = calculator.resultantValue();
        assertEquals(3, result, DEFAULT_PRECISION);
    }
    @Test
    public void theCalculatorShouldReturn1AsItsResult_whenWeFirstInput2AndThenSubtract1() {
        Calculator calculator = new Calculator();
        calculator.inputNumber(3);
        calculator.inputNumber(1);
    }
}
