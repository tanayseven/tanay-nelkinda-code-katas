package calculator;

/**
 * Created by tanay on 24/8/16.
 */
public class Calculator {

    private double result, input;

    public double resultantValue() {
        return this.result;
    }

    public void inputNumber(double number) {
        this.input = number;
    }

    public void addNumber() {
        this.result += this.input;
    }
}
