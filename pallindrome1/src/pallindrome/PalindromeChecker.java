package pallindrome;

public class PalindromeChecker {
    public static boolean isPalindrome(String string) {
        boolean isPalindrome = true;
        for (int i = 0, j = string.length()-1 ; i < j ; i++, j--) {
            if (string.charAt(i) != string.charAt(j)) {
                isPalindrome = false;
                break;
            }
        }
        return isPalindrome;
    }
}
