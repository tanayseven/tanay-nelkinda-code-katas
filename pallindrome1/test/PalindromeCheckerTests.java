import org.junit.Test;
import pallindrome.PalindromeChecker;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class PalindromeCheckerTests {
    @Test
    public void palindromeCheckerShouldReturnTrue_whenACorrectPalindromeStringIsPassed() {
        boolean result = PalindromeChecker.isPalindrome("MoM");
        assertTrue(result);
    }
    @Test
    public void palindromeCheckerShouldReturnFalse_whenACorrectPalindromeStringIsPassed() {
        boolean result = PalindromeChecker.isPalindrome("Akka");
        assertFalse(result);
    }
}
