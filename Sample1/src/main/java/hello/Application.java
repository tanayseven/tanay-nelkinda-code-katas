package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Arrays;

@SpringBootApplication
public class Application {
    public static void main(String [] commandArguments) {
        ApplicationContext context = SpringApplication.run(Application.class, commandArguments);

        System.out.println("Lets inspect the beans provided by Spring Boot");

        String[] beanNames = context.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for(String beanName: beanNames) {
            System.out.println(beanName);
        }
    }
}
